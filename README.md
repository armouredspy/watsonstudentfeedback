WatsonStudentFeedback README

Just clone this repository and open up index.html

Once done, enter a correct solution, wait a bit, then enter a student solution and watch the magic happen.

Sample correct solution:

refraction occurs as light passes across the boundary between two media. Refraction is merely one of several possible boundary behaviors by which a light wave could behave when it encounters a new medium or an obstacle in its path. The transmission of light across a boundary between two media is accompanied by a change in both the speed and wavelength of the wave. The light wave not only changes directions at the boundary, it also speeds up or slows down and transforms into a wave with a larger or a shorter wavelength. The only time that a wave can be transmitted across a boundary, change its speed, and still not refract is when the light wave approaches the boundary in a direction that is perpendicular to it. As long as the light wave changes speed and approaches the boundary at an angle, refraction is observed.

sample student solution:

refraction is the process of light changing it's path when encountering a new medium. Both it's speed and wavelength change such that it speeds up or slows down