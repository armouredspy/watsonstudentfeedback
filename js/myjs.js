var keywordArray = [];

var submitData = function() {
  var urlBase = "https://gateway-a.watsonplatform.net/calls/text/TextGetRankedKeywords?";
  var apiKey = "53c6fbe14a271b0431a913804df64b49dc331406";
  urlBase = urlBase + "apikey=" + apiKey;
  var inputText = $("#correct").val();
  urlBase = urlBase + "&text=" + encodeURIComponent(inputText);
  urlBase = urlBase + "&outputMode=json";

  console.log(urlBase);

  $.ajax({
    url: urlBase,
    type: 'POST',
    success: function(data) {
      console.log(data);
      if (keywordArray.length <= 0) {
         keywordArray = data.keywords;
         clearForm();
         $("#inputLabel").html("Submit a student response.");
         $("#outputDiv").html("Correct information received. Please input a sample");
      } else {
        processWatsonOutput(data.keywords);
      }
    }
  });


}

var clearForm = function() {
  $("#correct").val('');
}

var processWatsonOutput = function(watsonKeys) {
  //look at each keywords
  //try to find each keyword in stored keywords array
  //if we do find it, compare relevance scores
  //if we don't find it, then zero

  //define number of matches requred for success
  var numberOfRequiredMatches = watsonKeys.length / 2;
  matchedKeywordsArray = [];
  unmatchedKeywordsArray = [];
   //look at each key
  for (var i = 0; i < watsonKeys.length; i++) {
    var currentKeyText = watsonKeys[i].text;
    var foundKeyIndex;
    //find key in stored array
    for (var k = 0; k < keywordArray.length; k++) {
      if (keywordArray[k].text == currentKeyText) {
        foundKeyIndex = k;
        break;
      } else {

      }
    }
    //check to see if we found it
    if (foundKeyIndex >= 0) {
      //match is relevance within 0.15
      var difference = Math.abs(watsonKeys[i].relevance - keywordArray[foundKeyIndex].relevance);
      if (difference >= 0 && difference < 0.3) {
        //match
        matchedKeywordsArray.push(currentKeyText);
      } else {
        //no match

      }
    } else {

    }
  }

   //find unmatched keywords
   var seenWithoutMatch = new Object();
   for (var i = 0; i < keywordArray.length; i++) {
     seenWithoutMatch[keywordArray[i].text] = 1;
   }
   for (var i = 0; i < matchedKeywordsArray.length; i++) {
     seenWithoutMatch[matchedKeywordsArray[i]] = 0;
   }
   for (var key in seenWithoutMatch) {
     if (seenWithoutMatch.hasOwnProperty(key)) {
       if (seenWithoutMatch[key] == 1) {
         unmatchedKeywordsArray.push(key);
         console.log(key + "\n");
       }
     }
   }

   //get matched keywords into nice bullet point form
   var matchedText = "<ul>";
   for (var i = 0; i < matchedKeywordsArray.length; i++) {
     matchedText = matchedText + "<li>" + matchedKeywordsArray[i] + "</li>";
   }
   matchedText = matchedText + "</ul>";
   //get unmatched keywords into nice bullet point form
   var unmatchedText = "<ul>";
   for (var i = 0; i < unmatchedKeywordsArray.length; i++) {
     unmatchedText = unmatchedText + "<li>" + unmatchedKeywordsArray[i] + "</li>";
   }
   unmatchedText = unmatchedText + "</ul>";

  //provide feedback
  if (matchedKeywordsArray.length >= numberOfRequiredMatches) {


    //we found keyword match
    $("#outputDiv").html("We've found a match. You may want to talk more about: <br>" + unmatchedText + "You talked well about: <br>" + matchedText);
  } else {
    //no keyword match
    $("#outputDiv").html("no match found. You may want to talk more about: <br> " + unmatchedText);
  }

}
